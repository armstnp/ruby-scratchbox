# frozen_string_literal: true

require 'preparable'

# Implementation details:
# - Time is measured directly, not injected, thus reducing testability.
# - Each configurable part could be better vetted before accepting them.

class RetryPolicy
  include Preparable

  def initialize
    @name              = ''
    @past_max_attempts = ->(_) { false }
    @past_max_time     = ->(_) { false }
    @backoff_method    = ->(retry_number) { 2 ** retry_number + rand } # ~Exponential Backoff
    @should_retry      = ->(_) { raise "No retry predicate configured for retry policy: #{self}" }
    @wrapper           = ->(proc) { proc.call }
  end

  def with_name(name)
    @name = name
  end

  def with_max_attempts(max_attempts)
    @past_max_attempts = ->(attempts) { attempts >= max_attempts }
  end

  def with_max_time_secs(max_time_secs)
    @past_max_time = ->(elapsed_time) { elapsed_time >= max_time_secs }
  end

  def with_backoff_method(&block)
    @backoff_method = block
  end

  def retries_when(&block)
    @should_retry = block
  end

  def wrap_call_with(&block)
    @wrapper = block
  end

  def call(&block)
    start_time = Time.now

    (1..).each do |attempt|
      result = wrapper.call block
      return result unless should_retry.call result

      backoff           = backoff_method.call attempt
      next_attempt_time = Time.now + backoff
      time_spent        = next_attempt_time - start_time

      return result if past_max_attempts.call(attempt) || past_max_time.call(time_spent)

      sleep backoff
    end
  end

  private

  attr_reader :name, :past_max_attempts, :past_max_time, :backoff_method, :should_retry, :wrapper
end
