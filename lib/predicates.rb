# frozen_string_literal: true

module Predicates
  def self.exists_and(&block)
    ->(x) { !x.nil? && block.call(x) }
  end

  def self.nil_or(&block)
    ->(x) { x.nil? || block.call(x) }
  end
end
