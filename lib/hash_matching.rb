# frozen_string_literal: true

module HashMatching
  refine Hash do
    # Tests an indexable object for case-equality by applying this hash's
    # values as criteria for the target object's values at their corresponding
    # keys.
    #
    # @param other [Object] the target object to test
    # @return [Boolean] whether other is case-equal to this hash.
    def ===(other)
      other.respond_to?(:[]) && all? { |k, v| v === other[k] }
    end
  end
end
