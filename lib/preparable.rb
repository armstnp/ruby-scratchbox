# frozen_string_literal: true

# Implementation notes:
# - This doesn't play well with writer methods. I'm not even certain `self.x = ...`, and I haven't
#   bothered to try. This style seems to work best in a class using more of a builder method
#   pattern.

# Enables a class to initialize instances with a self-bound block.
#
# Example:
#   class A
#     include Preparable
#     def do_thing
#       ...
#   end
#
#   A.prepare do
#     do_thing
#     do_thing
#   end
module Preparable
  def self.included(base)
    base.extend Builder
  end

  module Builder
    # Creates a new instance of this class, passing the given args to `#new`; runs the given block -
    # if any - by evaluating it as the new object; and returns the newly created and prepared
    # object.
    # @example Preparing a new instance
    #   class A
    #     include Preparable
    #     def do_thing
    #       ...
    #   end
    #
    #   A.prepare do
    #     do_thing
    #     do_thing
    #   end
    def prepare(*args, &block)
      inst = new(*args)
      inst.instance_eval(&block) if block_given?
      inst
    end
  end
end
